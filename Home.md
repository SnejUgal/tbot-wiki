Welcome to `tbot`'s wiki! Here's what you can find here.

## [Tutorial]

[tutorial]: ./Tutorial

With our tutorial, you will learn how to write bots with `tbot` and the basics
of its API and design.

## [How-to guides]

[how-to guides]: ./How-to

We prepared a few how-to guides which explain several advanced `tbot` use cases.

## [Projects built with `tbot`]

[projects built with `tbot`]: ./Projects-built-with-tbot

Though not more popular than other crates yet, `tbot` is already used to build
bots and programs for the real world. This wiki page is a list of projects that
alredy use `tbot`.

## [`tbot`'s design][design]

[design]: ./Design-philosophy

`tbot` has made several design decisions, which are written down and described
in a separate wiki page.

---

Here are several links you may be interested in as well:

- We have a [policy on breaking changes][breaking changes], as there are a few
  breaking change cases we won't consider needing a major version bump.
- If you need a reference for `tbot`'s API, you can find it on
  [docs.rs][api reference].

[breaking changes]: ./Breaking-changes
[api reference]: https://docs.rs/tbot

You can also contribute to our wiki on [GitLab][git] — feel free to file
issues and merge requests!

[git]: https://gitlab.com/SnejUgal/tbot-wiki
