Even though `tbot` follows [semantic versioning][semver] and [RFC 1105]
in general, there are several breaking changes we don't consider as needing
a major version bump.

[semver]: https://semver.org
[rfc 1105]: https://github.com/rust-lang/rfcs/blob/master/text/1105-api-evolution.md

## Upstream breaking changes

If Telegram releases a new API version which causes a breaking change in a
backwards-incompatible way, we won't do a major version bump — your current code
is likely not to work properly anyway.

## Bug fixes

We don't consider bug fixes needing a major version bump even if the fix causes
a breaking change — your bot didn't work properly anyway before the fix.
